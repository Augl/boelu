package mining

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/augl/boelu/database"
	"bitbucket.org/augl/boelu/model"
	"golang.org/x/net/html"
)

// Mine gets all the ticket prices from the remote server.
func Mine() error {
	database := database.Database{}
	error := database.Open()
	if error != nil {
		return error
	}
	channel := make(chan model.PriceResult)
	startDate := time.Now().AddDate(0, 0, 2)
	for i := 0; i < 14; i++ {
		endDate := startDate.AddDate(0, 0, 7)
		go getFlightPrices(channel, startDate, endDate)
		startDate = startDate.AddDate(0, 0, 1)
	}
	for i := 0; i < 14*2; i++ {
		result := <-channel
		if result.Price == 2000 {
			return ErrorUnexpectedResponse{"Min price is 2000, something must have gone wrong!"}
		}
		error := database.StorePriceResult(result)
		if error != nil {
			return error
		}
	}
	return nil
}

func getFlightPrices(channel chan model.PriceResult, startDate, endDate time.Time) {
	body, err := sendPost(startDate.Format("02.01.2006"), endDate.Format("02.01.2006"))
	t := time.Now()
	if err != nil {
		fmt.Println(err.Error())
		channel <- model.PriceResult{Date: startDate, Price: 2000, IsTo: true, MiningDate: t}
		return
	}
	priceTo, priceFrom, error := crawlSource(body)
	if error != nil {
		fmt.Println(error.Error())
		channel <- model.PriceResult{Date: endDate, Price: 2000, IsTo: false, MiningDate: t}
		return
	}
	channel <- model.PriceResult{Date: startDate, Price: priceTo, IsTo: true, MiningDate: t}
	channel <- model.PriceResult{Date: endDate, Price: priceFrom, IsTo: false, MiningDate: t}
}

func crawlSource(body io.ReadCloser) (float64, float64, error) {
	var pricesTo []float64
	var pricesBack []float64
	isReturn := false
	tokenizer := html.NewTokenizer(body)
	defer body.Close()
	for {
		tokenType := tokenizer.Next()
		token := tokenizer.Token()
		switch {
		case tokenType == html.ErrorToken:
			return findMin(pricesTo), findMin(pricesBack), nil
		case tokenType == html.StartTagToken:
			// Checks if the token is a header3.
			if token.Data == "h3" {
				tokenizer.Next()
				token = tokenizer.Token()
				// If the content is Return we can start collecting return prices.
				if token.Data == "Return" {
					isReturn = true
				}
			}
			for _, a := range token.Attr {
				if a.Key == "class" {
					if a.Val == "av-price" {
						price, error := readPrice(token, tokenizer)
						if error != nil {
							fmt.Println("ERROR: Error crawling page")
							return 0, 0, error
						}
						if isReturn {
							pricesBack = append(pricesBack, price)
						} else {
							pricesTo = append(pricesTo, price)
						}
					}
				}
			}
		}
	}
}

func readPrice(token html.Token, tokenizer *html.Tokenizer) (float64, error) {
	priceString := ""
	// Goes to the next token.
	tokenizer.Next()
	// Gets the token containing the price.
	token = tokenizer.Token()
	priceString += token.Data
	// Skips next span token.
	tokenizer.Next()
	// Goes to the next text node.
	tokenizer.Next()
	// Gets the token containing cents value.
	token = tokenizer.Token()
	priceString += "." + token.Data
	// Tries to convert the constructed string into a Float64
	price, error := strconv.ParseFloat(priceString, 64)
	if error != nil {
		fmt.Println("Error converting string to int")
		return 0, error
	}
	// Returns the price.
	return price, nil
}

func findMin(slice []float64) float64 {
	min := 2000.0
	for _, v := range slice {
		if v < min {
			min = v
		}
	}
	return min
}

func sendPost(start, end string) (io.ReadCloser, error) {
	targetURL := "https://tickets.airbaltic.com/en/book/avail"
	form := url.Values{}
	form.Set("action2", "avail")
	form.Add("width", "1370")
	form.Add("height", "470")
	form.Add("p", "bti")
	form.Add("pos", "CH")
	form.Add("l", "en")
	form.Add("traveltype", "bti")
	form.Add("origin", "ZRH")
	form.Add("origin_type", "A")
	form.Add("destin", "RIX")
	form.Add("destin_type", "A")
	form.Add("numadt", "1")
	form.Add("numchd", "0")
	form.Add("numinf", "0")
	form.Add("bbv", "0")
	form.Add("flt_origin_text", "Zurich (Kloten) (ZRH) - Switzerland")
	form.Add("flt_destin_text", "Riga (RIX) - Latvia")
	form.Add("sref", "")
	form.Add("legs", "2")
	form.Add("flt_leaving_on", start)
	form.Add("flt_returning_on", end)
	client := &http.Client{}
	formString := form.Encode()
	request, error := http.NewRequest("POST", targetURL, strings.NewReader(formString))
	if error != nil {
		fmt.Println("Error creating request")
		return nil, error
	}
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	error = nil
	resp, error := client.Do(request)
	if error != nil {
		fmt.Println("Error sending request")
		return nil, error
	}
	if resp.StatusCode == http.StatusOK {
		return resp.Body, nil
	}
	return nil, &errorPostRequest{resp.StatusCode}
}

type errorPostRequest struct {
	statusCode int
}

func (err errorPostRequest) Error() string {
	return "Error while sending post request. Received status " + fmt.Sprintf("%v", err.statusCode)
}

// ErrorUnexpectedResponse occurs if the remote server returns an unexpected page.
type ErrorUnexpectedResponse struct {
	message string
}

func (err ErrorUnexpectedResponse) Error() string {
	return "ERROR: " + err.message
}
