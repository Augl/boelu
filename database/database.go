package database

import (
	"database/sql"
	"time"

	"bitbucket.org/augl/boelu/model"
	// Imports the SQLite3 driver.
	_ "github.com/mattn/go-sqlite3"
)

// Database handles the connection to the sqlite file.
type Database struct {
	connection *sql.DB
}

// Open starts the connection to the SQLite database.
// The needed tables are created if they do not exist yet.
func (db *Database) Open() error {
	con, error := sql.Open("sqlite3", "./boelu.db")
	if error != nil {
		return error
	}
	db.connection = con
	statement, error := db.connection.Prepare("CREATE TABLE IF NOT EXISTS dataset (id INTEGER PRIMARY KEY, date DATETIME, is_to INTEGER, price FLOAT, timestamp DATETIME)")
	if error != nil {
		return error
	}
	statement.Exec()
	statement, error = db.connection.Prepare("CREATE TABLE IF NOT EXISTS minings (id INTEGER PRIMARY KEY, success INTEGER, timestamp DATETIME)")
	if error != nil {
		return error
	}
	statement.Exec()
	defer statement.Close()
	return nil
}

// StorePriceResult puts the given PriceResult into the database.
func (db *Database) StorePriceResult(result model.PriceResult) error {
	statement, error := db.connection.Prepare("INSERT INTO dataset (date, is_to, price, timestamp) VALUES (?,?,?,?)")
	defer statement.Close()
	if error != nil {
		return error
	}
	_, error = statement.Exec(result.Date, result.IsTo, result.Price, result.MiningDate)
	if error != nil {
		return error
	}
	return nil
}

// GetAllPriceResults returns all PriceResult objects that are stored in the database.
func (db *Database) GetAllPriceResults() ([]model.PriceResult, error) {
	rows, error := db.connection.Query("SELECT * FROM dataset ORDER BY date")
	if error != nil {
		return nil, error
	}
	defer rows.Close()
	var result []model.PriceResult
	var id int
	var price float64
	var date, miningDate time.Time
	var isTo bool
	for rows.Next() {
		rows.Scan(&id, &date, &isTo, &price, &miningDate)
		result = append(result, model.PriceResult{
			ID:         id,
			IsTo:       isTo,
			Date:       date,
			MiningDate: miningDate,
			Price:      price})
	}
	return result, nil
}

// StoreMiningResult stores the result of the mining try in the database.
func (db *Database) StoreMiningResult(success bool) error {
	statement, error := db.connection.Prepare("INSERT INTO minings (success, timestamp) VALUES (?, ?)")
	defer statement.Close()
	if error != nil {
		return error
	}
	_, error = statement.Exec(success, time.Now())
	if error != nil {
		return error
	}
	return nil
}

// GetMiningResults returns a slice containing all mining results.
func (db *Database) GetMiningResults() ([]model.MiningResult, error) {
	rows, error := db.connection.Query("SELECT * FROM minings ORDER BY timestamp DESC")
	if error != nil {
		return nil, error
	}
	defer rows.Close()
	var result []model.MiningResult
	var id int
	var success bool
	var date time.Time
	for rows.Next() {
		rows.Scan(&id, &success, &date)
		result = append(result, model.MiningResult{
			ID:      id,
			Success: success,
			Date:    date.Format("02.01.2006 15:04")})
	}
	return result, nil
}
