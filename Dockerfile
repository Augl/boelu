FROM node:alpine AS build-env

WORKDIR /app
COPY ./frontend ./
RUN apk update && \
    apk upgrade && \
    apk add git && \
    npm install -g polymer-cli --unsafe-perm && \
    npm install -g bower && \
    mkdir app && \
    bower install --allow-root && \
    polymer build

FROM golang:alpine

COPY --from=build-env /app/build/es5-bundled /frontend/

RUN apk update && apk add git && apk add build-base

ADD . /go/src/bitbucket.org/augl/boelu

RUN go get github.com/mattn/go-sqlite3

RUN go get golang.org/x/net/html

RUN go install bitbucket.org/augl/boelu/main

ENTRYPOINT ["/go/bin/main"]

EXPOSE 8080