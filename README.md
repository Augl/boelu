#Boelu

...

## Endpoints
### GET /api/mine
Mine connects to the BalticAir ticketing system and crawls the page for ticket prices.
The results are stored in the database.
### GET /api/prices/average
Returns the average flight prices for every date.

## Docker-Compose
```
version: "3"

services:
  boelu:
    image: augl/boelu:latest
    container_name: boelu
    restart: always
    networks:
      - web
    expose:
      - "8080"
    labels:
      - "traefik.backend=Boelu"
      - "traefik.docker.network=web"
      - "traefik.frontend.rule=Host:boelu.lhsoftware.ch"
      - "traefik.enable=true"
      - "traefik.port=8080"
      - "traefik.frontend.headers.SSLRedirect=true"
    volumes:
      - ./boelu.db:/go/bin/boelu.db

networks:
  web:
    external: true
```