package model

// MiningResult is used to display the success / failure of a crawl on the GUI.
type MiningResult struct {
	ID      int    `json:"id"`
	Date    string `json:"date"`
	Success bool   `json:"success"`
}
