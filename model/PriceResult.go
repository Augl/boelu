package model

import (
	"fmt"
	"time"
)

// PriceResult holds all significant information of a ticket price.
type PriceResult struct {
	ID         int
	Date       time.Time
	Price      float64
	IsTo       bool
	MiningDate time.Time
}

func (pr PriceResult) String() string {
	return fmt.Sprintf("id: %v, date: %v, price: %v, isTo: %v, mining: %v", pr.ID, pr.Date, pr.Price, pr.IsTo, pr.MiningDate)
}
