package model

// AveragePrice is used for the AveragePrice chart in the gui.
// Contains the date of the flight, the average price to fly to Riga and the average price to fly back.
type AveragePrice struct {
	Date      string  `json:"date"`
	PriceTo   float64 `json:"priceTo"`
	PriceFrom float64 `json:"priceFrom"`
}
