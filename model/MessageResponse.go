package model

// MessageResponse is a simple structure holding a message string.
type MessageResponse struct {
	Message string
}
