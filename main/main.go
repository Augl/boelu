package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"sort"

	"bitbucket.org/augl/boelu/database"
	"bitbucket.org/augl/boelu/mining"
	"bitbucket.org/augl/boelu/model"
)

func main() {

	http.HandleFunc("/api/mine", mine)
	http.HandleFunc("/api/mine/results", getMiningResults)
	http.HandleFunc("/api/prices/average", getAveragePrices)
	http.Handle("/", http.FileServer(http.Dir("../frontend")))

	log.Fatal(http.ListenAndServe(":8080", nil))
}

// mine calls the Mine function of the mining module and stores the result in the database.
func mine(writer http.ResponseWriter, request *http.Request) {
	database := database.Database{}
	error := database.Open()
	if error != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		fmt.Println(error.Error())
		return
	}
	error = mining.Mine()
	var message string
	if error != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		fmt.Println(error.Error())
		message = "Internal Server Error"
		database.StoreMiningResult(false)
	} else {
		writer.WriteHeader(http.StatusOK)
		message = "Success"
		database.StoreMiningResult(true)
	}
	response := model.MessageResponse{Message: message}
	responseBytes, _ := json.Marshal(response)
	writer.Write(responseBytes)
}

func getAveragePrices(writer http.ResponseWriter, request *http.Request) {
	if request.Method != "GET" {
		writer.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	writer.Header().Add("Access-Control-Allow-Origin", "http://localhost:8081") //TODO: Remove
	writer.Header().Add("Content-Type", "application/json")
	database := database.Database{}
	error := database.Open()
	if error != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		fmt.Println(error.Error())
		return
	}
	prices, error := database.GetAllPriceResults()
	if error != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		fmt.Println(error.Error())
		return
	}
	pricesTo := make(map[string][]float64)
	pricesFrom := make(map[string][]float64)
	datesMap := make(map[string]bool)
	for _, v := range prices {
		date := v.Date.Format("02.01.2006")
		datesMap[date] = true
		if v.IsTo {
			arrTo := pricesTo[date]
			arrTo = append(arrTo, v.Price)
			pricesTo[date] = arrTo
		} else {
			arrFrom := pricesFrom[date]
			arrFrom = append(arrFrom, v.Price)
			pricesFrom[date] = arrFrom
		}
	}
	var result []model.AveragePrice
	for k := range datesMap {
		var avgTo float64
		var avgFrom float64
		if len(pricesTo[k]) > 0 {
			sumTo := 0.0
			for _, v := range pricesTo[k] {
				sumTo += v
			}
			avgTo = sumTo / float64(len(pricesTo[k]))
		}
		if len(pricesFrom[k]) > 0 {
			sumFrom := 0.0
			for _, v := range pricesFrom[k] {
				sumFrom += v
			}
			avgFrom = sumFrom / float64(len(pricesFrom[k]))
		}
		result = append(result,
			model.AveragePrice{
				Date:      k,
				PriceTo:   math.Round(avgTo*100) / 100,
				PriceFrom: math.Round(avgFrom*100) / 100})
	}
	sort.Slice(result, func(i, j int) bool {
		return result[i].Date < result[j].Date
	})
	message, error := json.Marshal(result)
	if error != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		fmt.Println(error.Error())
		return
	}
	writer.Write(message)
}

func getMiningResults(writer http.ResponseWriter, request *http.Request) {
	if request.Method != "GET" {
		writer.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	writer.Header().Add("Access-Control-Allow-Origin", "http://localhost:8081") //TODO: Remove
	writer.Header().Add("Content-Type", "application/json")
	database := database.Database{}
	error := database.Open()
	if error != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		fmt.Println(error.Error())
		return
	}
	result, error := database.GetMiningResults()
	if error != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		fmt.Println(error.Error())
		return
	}
	message, error := json.Marshal(result)
	if error != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		fmt.Println(error.Error())
		return
	}
	writer.Write(message)
}
